Write-Host "It's ALIVE!"

Install-Module -Name AWSPowershell.NetCore -Force
Import-Module -Name AWSPowershell.NetCore -Force

Set-AWSCredential -AccessKey $env:AWS_ACCESS_KEY -SecretKey $env:AWS_SECRET_KEY -StoreAs default

Get-S3Bucket -BucketName bawksbucketforstuff
